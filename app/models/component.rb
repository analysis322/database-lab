class Component < ActiveRecord::Base
  establish_connection "production"
  belongs_to :company

  validates :name, :db_name, presence: true
end