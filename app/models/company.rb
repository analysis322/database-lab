class Company < ActiveRecord::Base
  establish_connection "production"
  has_many :brands, dependent: :destroy

  validates :subdomain, :db_user, :db_host, :db_port, :name, presence: true
end