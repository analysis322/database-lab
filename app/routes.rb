# -*- coding: utf-8 -*-
# app/routes.rb

get '/' do
  @title = 'Главная страница'
  erb :index
end

get '/catalog' do
  @title = 'Компьютеры и переферия'
  erb :catalog
end

get '/catalog_components' do
  @title = 'Комплетующие'
  erb :catalog_components
end

get '/choice' do
  @title = 'Комплетующие'
  erb :choice
end