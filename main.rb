# main.rb
# -*- coding: utf-8 -*-

require 'sinatra'
require 'sinatra/activerecord'
require 'redcarpet'

Dir.glob('./{app,config}/*.rb').each {|file| require file}
